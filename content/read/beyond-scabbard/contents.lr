title: Beyond Scabbard
---
pub_date: 2020-05-28
---
author: vimino
---
twitter_handle: vimino_net
---
body:

[0]: https://www.mysql.com/products/community/
[1]: https://www.mysql.com/products/workbench/
[2]: https://www.getlektor.com/
[3]: https://en.wikipedia.org/wiki/Content_management_system
[4]: https://jinja.palletsprojects.com/en/2.11.x/
[5]: https://flask.palletsprojects.com/en/1.1.x/
[6]: https://daringfireball.net/projects/markd0own/
[7]: https://www.python.org/
[8]: https://www.staticgen.com/
[9]: https://www.merriam-webster.com/dictionary/win-win

So you know how I said *scabbard* was rather dreadful to use?

Well, here are some excerpts from its own *help* command':
- List news: `./scabbard.py --list news`
- Add a project: `./scabbard.py --add projects/project title='TITLE' date='DATE' type='page/document/link' uri='file.html'`
- Edit a blog post: `./scabbard.py --edit blog/entry ID title='TITLE' date='DATE/DATETIME'`

Note that *scabbard* managed the content on a [*MySQL*][0] database so I could also use the [*(MySQL) Workbench*][1] but it wasn't much better.

Thankfully, this all changed when I found [**Lektor**][2]. What is it? A [CMS][3] for static websites which: Get this, uses [jinja2 templates][4] (that I already used in [Flask][5]), supports [markdown][6] pages and effectively replaces **scabbard**. With the addition of *previews*, *spellchecking* and easy *content management*!<br/>
So it effectively uses [Python][7] behind the scenes? **I don't care** about the details, **I can focus on producing content** without worrying about [which Static Site Generator to use][8] or what I'd have to learn to use it. I'd call that a [win-win][0].
